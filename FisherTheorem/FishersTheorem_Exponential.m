close all;
clear all;
clc;

Colours = [ 0 0 0;... % Black
            1.0 0.6 0.4;... %Salmon
            0 0.65 0.65;...  % Turquoise?
            0.75 0.75 0.75 ];

%% Common parameters
x = 0:0.1:24;
y0 = 0.001;

r1 = 0.15;
r2 = 0.2;

%% Exponential model. Here r > 0 denotes growth.
n1 = y0 .* exp(x .* r1);
n2 = y0 .* exp(x .* r2);

% Measure fitness
m1 = log2(n1./n1(1)) ./ (x - x(1));
m2 = log2(n2./n2(1)) ./ (x - x(1));

fig_1a = figure(1);  % Alternative Figure
    set(fig_1a, 'Color', 'w', 'Position', [30 400 600 500], 'Visible', 'on');
    set(gcf, 'PaperUnits', 'points');

	W = m2./m1;  % Relative fitness
    	f = plot(x, W, 'color', Colours(1, :), 'linewidth', 4);
	hold on;
		ref = plot([x(1), x(end)], [1 1], 'color', 'black',...
			   'linewidth', 1.65, 'linestyle', ':');
	hold off

	% Axes
	ylim([0.95 1.45]);
	xlim([x(1), x(end)]);
	xlabel('Time (h)', 'FontSize', 22);
	ylabel('Relative fitness of mutant B', 'FontSize', 22);
	set(gca, 'FontSize', 20, 'Units', 'points');

	% Annotations
	annotation('doublearrow', [0.2 0.2], [0.24 0.725],...
		       'Head2Style', 'plain', 'Head1Style', 'plain',...
		       'Head2Width', 4, 'Head2Length', 4,...
		       'Head1Width', 4, 'Head1Length', 4);     
	text(2.5, 1.175, ['W_{ba} = {\it k}'], 'FontSize', 18,...
		 'HorizontalAlignment', 'left');


	% Inset
	axes('Parent', fig_1a, 'Position',[0.575 0.37 0.3 0.33]);

	p1 = plot(x, n1, 'color', Colours(3,:) * 1.25, 'linewidth', 3);
	hold on;
	p2 = plot(x, n2, 'color', Colours(1,:), 'linewidth', 3);
	hold off;

	% Legend
	l = legend([p1, p2], {'Mutant A'; 'Mutant B'});
		set(l, 'Location', 'NorthWest', 'box', 'off', 'FontSize', 16);

	% Axes
	box on;
	axis tight;
	xlabel('Time (h)', 'FontSize', 16);
	ylabel('Culture density (A.U.)', 'FontSize', 16);
	ylims = get(gca, 'YLim');
	set(gca, 'YTick', [0:0.1:0.2], 'YLim', [0, ylims(end)]);

	% Annotations
	set(gca, 'FontSize', 16, 'Units', 'points');

	% Save
	saveas(fig_1a, './img/Fitness_Model_Exponential_Alt.eps', 'epsc');
	close(fig_1a);

%% Propagate Exponential
n1_0 = y0;
n2_0 = y0;

dilution_factor = 0.01;

t_init = 61;
t_step = 60;
sampling_t = [t_init:t_step:length(x)];
Days = 7;

n1_3D = [];
n2_3D = [];

for t_s = sampling_t
    n1_prop = [];
    n2_prop = [];
    for day = 1:Days
    	n1 = n1_0 .* exp(x .* (r1-.9));
    	n2 = n2_0 .* exp(x .* (r2-.9));
    	% Store
    	n1_prop = [n1_prop; n1(t_s)];
    	n2_prop = [n2_prop; n2(t_s)];
    	% Update initial conditions
    	n1_0 = n1(t_s) * dilution_factor;
    	n2_0 = n2(t_s) * dilution_factor;
    end
    n1_3D = [n1_3D, n1_prop];
    n2_3D = [n2_3D, n2_prop];
end

fig_1b = figure(1);  % 3D Plot
    set(fig_1b, 'Color', 'w', 'Position', [30 400 675 650], 'Visible', 'on');
    set(gcf, 'PaperUnits', 'points');

	sampling_n = length(x(t_init:t_step:end));
	Ntot_3D = n1_3D + n2_3D;

	p1 = plot3(24 * repmat(1:Days, sampling_n, 1)',...
		   repmat(x(sampling_t), Days, 1),...
		   n1_3D ./ Ntot_3D, 'color', Colours(3, :) * 1.25, 'linewidth', 4);
	hold on;
		p2 = plot3(24 * repmat(1:Days, sampling_n, 1)',...
			   repmat(x(sampling_t), Days, 1),...
			   n2_3D ./ Ntot_3D, 'color', Colours(1, :), 'linewidth', 4);
	hold off;

	% Axis
	axis tight;
	zlim([0, 1]);
	view([65 20]);
	grid on;

	xlabel('Time (h)', 'FontSize', 16);
	ylabel('Growth cycle (h)', 'FontSize', 16);
	zlabel('Relative frequency', 'FontSize', 16);

        set(gca, 'FontSize', 16, 'Units', 'points');
	set(gca, 'XTick', [24 * [1:2:Days]], 'linewidth', 0.5);

	% Legend
	l = legend([p1(1), p2(1)], {'Mutant A'; 'Mutant B'});
		set(l, 'Location', 'East', 'box', 'off', 'FontSize', 24);

	% Save
	saveas(fig_1b, './img/Fitness_Model_Exponential_Evol_3D.eps', 'epsc');
	close(fig_1b);

