close all;
clear all;
clc;

Colours = [ 0 0 0;... % Black
            1.0 0.6 0.4;... %Salmon
            0 0.65 0.65;...  % Turquoise?
            0.75 0.75 0.75 ];

%% Logistic model  (NO trade-off). Here r > 1 denotes growth.
x = 0:0.1:48;
y0 = 0.001;
n1 = zeros(size(x));
n1(1) = y0;

n2 = zeros(size(x));
n2(1) = y0;

r1 = 1.0275;
r2 = 1.05;

K1 = 5;
K2 = 5;
alpha = 0.15;

% ODE
for t = 2:length(x)
    n1(t) = r1 * n1(t-1) * (1 - (n1(t-1) - alpha*n2(t-1)) / K1);
    n2(t) = r2 * n2(t-1) * (1 - (n2(t-1) - alpha*n1(t-1)) / K2);
end

% Measure fitness
m1 = log2(n1./n1(1)) ./ (x - x(1));
m2 = log2(n2./n2(1)) ./ (x - x(1));



fig_3 = figure(3);  % Alternative figure
    set(fig_3, 'Color', 'w', 'Position', [300 400 600 500], 'Visible', 'on');
    set(gcf, 'PaperUnits', 'points');

	W = m2./m1;  % Relative fitness
	plot(x, m2./m1, 'color', Colours(1,:), 'linewidth', 4);
	hold on;
		plot([x(1), x(end)], [1 1], 'color', 'black',...
			'linewidth', 1.65, 'linestyle', ':');
		s = plot(x(241), W(241), 'marker', '.', 'markersize', 32,...
		     'color', 'red');
	hold off;

	% Axes
	ylim([0.9 1.65]);
	xlim([10, x(end)]);
	xlabel('Time (h)', 'FontSize', 22);
	ylabel('Relative fitness of mutant B', 'FontSize', 22);
	set(gca, 'FontSize', 20);

	% Annotations
	annotation('doublearrow', [0.17 0.17], [0.345 0.2575],...
		       'Head2Style', 'rectangle', 'Head1Style', 'plain',...
		       'Head2Width', 4, 'Head2Length', 0.5,...
		       'Head1Width', 4, 'Head1Length', 4);
	text(13, 1.05, ['W_{ba} \geq 1'], 'FontSize', 18);

	% Inset
	axes('Parent', fig_3, 'Position', [0.55 0.525 0.3 0.33]);

	p1 = plot(x, n1, 'color', Colours(3,:) * 1.25, 'linewidth', 3);
	hold on;
		p2 = plot(x, n2, 'color', Colours(1,:), 'linewidth', 3);
		plot([24, 24], [y0, n2(end)], 'color', Colours(4, :) * 0.75);
	hold off;

	% Axes
	axis tight;
	box on;
	ylims = get(gca, 'YLim');
	set(gca, 'YLim', [0 ylims(end)]);
	xlabel('Time (h)', 'FontSize', 16, 'VerticalAlignment', 'top');
	ylabel('Culture density (A.U.)', 'FontSize', 16, 'VerticalAlignment', 'bottom');
	set(gca, 'FontSize', 16, 'Units', 'points');

	% Annotations
	text(23, 0.075, '{\it t^*}', 'FontSize', 24);

	% Save
	saveas(fig_3, './img/Fitness_Model_Logistic_Alt.eps', 'epsc');
	close(fig_3);

%% Propagate logistic model  (NO trade-off)
x = 0:0.1:48;
n1_0 = y0;
n2_0 = y0;

n1(1) = n1_0;
n2(1) = n2_0;

t_init = 61;
t_step = 120;
sampling_t = [t_init:t_step:length(x)];
Days = 7;
dilution_factor = 0.01;

n1_3D = [];
n2_3D = [];

for t_s = sampling_t
    n1_prop = [];
    n2_prop = [];
    for day = 1:Days
    	% ODE
    	for t = 2:length(x)
    		n1(t) = r1 * n1(t-1) * (1 - (n1(t-1) - alpha*n2(t-1)) / K1);
    		n2(t) = r2 * n2(t-1) * (1 - (n2(t-1) - alpha*n1(t-1)) / K2);
    	end
    	% Store
    	n1_prop = [n1_prop; n1(t_s)];
    	n2_prop = [n2_prop; n2(t_s)];
    	% Update initial conditions
    	n1(1) = n1(t_s) * dilution_factor;
    	n2(1) = n2(t_s) * dilution_factor;
    end
    n1_3D = [n1_3D, n1_prop];
    n2_3D = [n2_3D, n2_prop];
end

fig_3b = figure(1);  % 3D Plot
    set(fig_3b, 'Color', 'w', 'Position', [30 400 675 650], 'Visible', 'on');
    set(gcf, 'PaperUnits', 'points');

	sampling_n = length(x(t_init:t_step:end));
	Ntot_3D = n1_3D + n2_3D;

	p1 = plot3(24 * repmat(2 * [1:Days] - 1, sampling_n, 1)',...
		   repmat(x(sampling_t), Days, 1),...
		   n1_3D ./ Ntot_3D, 'color', Colours(3, :) * 1.25, 'linewidth', 4);
	hold on;
		p2 = plot3(24 * repmat(2 * [1:Days] - 1, sampling_n, 1)',...
			   repmat(x(sampling_t), Days, 1),...
			   n2_3D ./ Ntot_3D, 'color', Colours(1, :), 'linewidth', 4);
	hold off;

	% Axis
	axis tight;
	zlim([0, 1]);
	view([65 20]);
	grid on;

	xlabel('Time (h)', 'FontSize', 16);
	ylabel('Growth cycle (h)', 'FontSize', 16);
	zlabel('Relative frequency', 'FontSize', 16);

        set(gca, 'FontSize', 16, 'Units', 'points');
	set(gca, 'XTick', [24 * (2 * [1:2:Days] - 1)], 'linewidth', 0.5);

	% Legend
	l = legend([p1(1), p2(1)], {'Mutant A'; 'Mutant B'});
		set(l, 'Location', 'NorthEast', 'box', 'off', 'FontSize', 24);

	% Save
	saveas(fig_3b, './img/Fitness_Model_Logistic_Evol_3D.eps', 'epsc');
	close(fig_3b);

%% Reproducing frequency-dependent fitness with Wij
t_init = 61;
t_step = 120;
sampling_t = [t_init:t_step:length(x)];
Days = 7;
dilution_factor = 0.01;

n1_3D = [];
n2_3D = [];

n1_f_freq = [];
n2_f_freq = [];

n1_init_od = [];
n2_init_od = [];

n1_f_od = [];
n2_f_od = [];

init_prop = 0:0.05:10;

parfor F = init_prop
	n1_0 = y0*F;
	n2_0 = y0;

	n1(1) = n1_0;
	n2(1) = n2_0;
	
	n1_init_od = [n1_init_od; n1_0];
	n2_init_od = [n2_init_od; n2_0];

	for t_s = sampling_t
	    n1_od = [];
	    n2_od = [];
	    for day = 1:Days
	    % ODE
	    	for t = 2:length(x)
	    		n1(t) = r1 * n1(t-1) * (1 - (n1(t-1) - alpha*n2(t-1)) / K1);
	    		n2(t) = r2 * n2(t-1) * (1 - (n2(t-1) - alpha*n1(t-1)) / K2);
	    	end
	    	% Store
	    	n1_od = [n1_od; n1(t_s)];
	    	n2_od = [n2_od; n2(t_s)];
	    	% Update initial conditions
	    	n1(1) = n1(t_s) * dilution_factor;
	    	n2(1) = n2(t_s) * dilution_factor;
	    end
	    n1_3D = [n1_3D, n1_od];
	    n2_3D = [n2_3D, n2_od];
	    if t_s == sampling_t(3)
		n1_f_od = [n1_f_od; n1(t_s)];
		n2_f_od = [n2_f_od; n2(t_s)];
	    end
	end
end


fig_3c = figure(3);
    set(fig_3c, 'Color', 'w', 'Position', [300 400 600 500], 'Visible', 'on');
    set(gcf, 'PaperUnits', 'points');
	
	% Malthusian
	m1 = log(n1_f_od ./ n1_init_od) / Days;
	m2 = log(n2_f_od ./ n2_init_od) / Days;
	% Relative fitness
	W = m2./m1;
	
	total_init_od = n1_init_od + n2_init_od;
	total_f_od = n1_f_od + n2_f_od;
	
	plot([0 1], [1 1], 'color', Colours(4,:), 'linewidth', 2);
	hold on;
	plot(n1_init_od ./ total_init_od, W, 'color', Colours(1,:), 'linewidth', 4);
	hold off;

	% Axes
	axis tight;
	xlabel('Initial frequency of A', 'FontSize', 22);
	ylabel('Relative fitness of mutant A', 'FontSize', 22);
	set(gca, 'FontSize', 20);

	% Save
	saveas(fig_3c, './img/Fitness_Model_Logistic_FreqDependence_OD.eps', 'epsc');
	close(fig_3c);


fig_3d = figure(3);
    set(fig_3d, 'Color', 'w', 'Position', [300 400 600 500], 'Visible', 'on');
    set(gcf, 'PaperUnits', 'points');
	
	total_init_od = n1_init_od + n2_init_od;
	total_f_od = n1_f_od + n2_f_od;
	
	plot(n1_init_od(2:end) ./ total_init_od(2:end), n1_f_od(2:end) ./ total_f_od(2:end), 'color', Colours(1,:), 'linewidth', 4);
	
	% Axes
	ylim([0 1]);
	xlim([0 1]);
	xlabel('Initial frequency of A', 'FontSize', 22);
	ylabel('Relative fitness of mutant A', 'FontSize', 22);
	set(gca, 'FontSize', 20);

	% Save
	saveas(fig_3d, './img/Fitness_Model_Logistic_FreqDependence_Freq.eps', 'epsc');
	close(fig_3d);
