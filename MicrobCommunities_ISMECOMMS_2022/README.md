# What is this?

This repository contains the python implementation of
the model that I [published](https://www.nature.com/articles/s43705-022-00186-5)
in _ISME Communications_.

The code requires `numpy`, `scipy`, and `matplotlib` to
generate the predictions discussed in the paper, that is,
about the efficacy of a drug against a population (S1) when
it grows in pure culture conditions, and when they grow
alongside a second population from a different species (S2).
It can also be used to explore more complex scenarios where
multiple species growing alongside are exposed to a drug or 
any other toxic substance. To run it, simply type `./runMe.py`
from the terminal and it will generate the plots.

If you use the model and find it useful, feel free to
[DM me](https://twitter.com/rc_reding) to say hi or stamp 
a citation in your preprint/submission:

```
@article{Reding_2022,
	author = {Reding, Carlos},
	journal = {ISME Commun.},
	volume = {2},
	pages = {110},
	publisher = {Springer Nature},
	title = {Predicting the re-distribution of antibiotic molecules caused by inter-species interactions in microbial communities},
	year = {2022}}

```


Carlos

PD.- I tested the code in Linux and macOS, if you happen to
try it in Windows and encounter any problem drop me a line
and I will be happy to implement the changes needed.
