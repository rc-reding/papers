import numpy as np

# Import here `odeint` and `interp1d' to improve performance (multi-call)
from mdl import mixed_culture_ode
from scipy.integrate import odeint
from scipy.interpolate import interp1d

def _calculate_carbon(C_range, A_range, dr, MIC):
    """
        Calculate relative carbon content at the MIC. The method calculates
        the `A_range' indexes immediately above and below the MIC, and applies
        them to `C_range'.
        
        Arguments:
            A_range: array. Range of drug concentrations.
            C_range: array. Total carbon uptaken across `A_range'.
            dr: array. Microbial growth across `A_range' (dose-response).
            MIC: float. Minimum Inhibitory Concentration.
    """
    Rel_CiC = C_range/dr
    if np.size(np.where(A_range <= MIC)) == 0:
        L_ID = np.nan
    else:
        L_ID = np.where(A_range <= MIC)[0][-1]
    
    if np.size(np.where(A_range >= MIC)) == 0:
        U_ID = np.nan
    else:
        U_ID = np.where(A_range >= MIC)[0][0]
    
    if np.isnan(L_ID) or np.isnan(U_ID):
        # No MIC defined, return NaN
        return np.nan
    else:
        L_CiC = Rel_CiC[L_ID]
        U_Cic = Rel_CiC[U_ID]
    return np.mean([L_CiC, U_Cic])

def ode_solver(u0, t, p, Competitor_Type='Tolerant', Efflux_Pumps=0):
    """
        Solves ODE system in `f' and returns the solution.
        
        Arguments:
            f: Function containing an ODE system.
            u0: List containing the initial guesses for all the _variables_
                  of the model.
            t: Timespan to solve the ODE system as an array.
            p: List containing all the _parameters_ of the model.
    """
    solution, _ = odeint(mixed_culture_ode, u0, t, args=(p, Competitor_Type,
                        Efflux_Pumps), full_output=True, rtol=1e-6, atol=1e-6,
                        tfirst=True)
    return solution


def calculate_doseResponse(u0, t, p, numPoints, Competitor_Type='Tolerant',
                           Efflux_Pumps=0):
    """
        Calculate the change in cell density with different drug
        concentrations. Returns the dose response for each type of 
        microbe, and the range of drug concentrations used.
        
        The method will solve the model `f' using different drug concentrations
        and then retrieve the last density (at t[-1]). Then plot these final
        densities for each drug concentration used.
        
        The number of drug concentrations is determined by `numPoints' and the
        highest concentration by u0[-1].
        
        Assumptions: two types of microbe (S and R).
    """
    drugSupply = u0[-1]
    concentrations_range = np.linspace(0, drugSupply, num=numPoints)

    # Is this initial/unique iteration?
    if type(u0[0]) == float and type(u0[1]) == float:
        S0 = np.repeat(u0[0], numPoints)
        R0 = np.repeat(u0[1], numPoints)
    else:
        S0 = u0[0]
        R0 = u0[1]

    drS = list()
    drR = list()
    CiS = list()
    CiR = list()
    DiS = list()
    DiR = list()
    Denv = list()
    for i, drugSupply in enumerate(concentrations_range):
        u0[0] = S0[i]
        u0[1] = R0[i]
        u0[-1] = drugSupply
        sol = ode_solver(u0, t, p, Competitor_Type, Efflux_Pumps)
        drS.append(sol.T[0])  #
        drR.append(sol.T[1])  #
        CiS.append(sol.T[3])  #
        CiR.append(sol.T[4])  # DO NOT FORGET TO USE [-1] 
        DiS.append(sol.T[5])  # TO REPRESENT END POINT
        DiR.append(sol.T[6])  #
        Denv.append(sol.T[7]) #
    return drS, drR, DiS, DiR, Denv, CiS, CiR, concentrations_range


def collate_results(solutions, A_range, Inhibition, Competitor_Type=None,
                    Reference='Monoculture', Reference_MIC=None):
    """
        Extract MICs and DICs from dose-response data
    """
    
    S = list()
    R = list()
    S_atMIC = list()
    R_atMIC = list()
    MIC_S = list()
    MIC_R = list()
    Carbon_in_S = list()
    Carbon_in_R = list()
    CiS = list()
    CiR = list()
    Drug_in_S = list()
    Drug_in_R = list()
    Drug_outside = list()
    DiS = list()
    DiR = list()
    for sol in solutions:
        S.append(sol[0].get()[0])  # .get(0) = S, 1 R, 2 A_range
        R.append(sol[0].get()[1])  # .get(0) = S, 1 R, 2 A_range
        Drug_in_S.append(sol[0].get()[2])
        Drug_in_R.append(sol[0].get()[3])
        Drug_outside.append(sol[0].get()[4])
        Carbon_in_S.append(sol[0].get()[5])
        Carbon_in_R.append(sol[0].get()[6])
    S = np.array(S)
    R = np.array(R)
    Drug_in_S = np.array(Drug_in_S)
    Drug_in_R = np.array(Drug_in_R)
    Drug_outside = np.array(Drug_outside)
    Carbon_in_S = np.array(Carbon_in_S)
    Carbon_in_R = np.array(Carbon_in_R)
    
    if Reference == None:
        # Calculate IC and DiC for the S-type in monoculture conditions.
        for drS, DrugContent, CarbonContent in zip(S[:,:,-1], Drug_in_S[:,:,-1], Carbon_in_S[:,:,-1]):
            IC, Growth_IC =  calculate_MIC(drS, A_range, Inhibition)
            MIC_S.append(IC)
            S_atMIC.append(Growth_IC)
            # Drug per cell
            DiC_Calc = interp1d(A_range, DrugContent/drS, bounds_error=False)
            DiS.append(DiC_Calc(IC).tolist())
            # Carbon per cell @ MIC
            CiS.append(_calculate_carbon(CarbonContent, A_range, drS, IC))
        # Calculate IC for the R-type (NOTE: R is NOT inhibited, IC == N/A!!!)
        for drR, DrugContent, CarbonContent in zip(R[:,:,-1], Drug_in_R[:,:,-1], Carbon_in_R[:,:,-1]):
            IC, Growth_IC = calculate_MIC(drR, A_range, Inhibition)
            MIC_R.append(IC)
            R_atMIC.append(Growth_IC)
            # Drug per cell
            DiC_Calc = interp1d(A_range, DrugContent/drR, bounds_error=False)
            DiR.append(DiC_Calc(IC).tolist())
            # Carbon per cell @ MIC in Monoculture
            CiR.append(_calculate_carbon(CarbonContent, A_range, drR, IC))
    else:
        # Calculate IC for the S-type, and DiC using the MIC in monoculture
        for MIC_ID, zData in enumerate(zip(S[:,:,-1], Drug_in_S[:,:,-1], Carbon_in_S[:,:,-1])):
            drS, DrugContent, CarbonContent = zData
            IC, Growth_IC = calculate_MIC(drS, A_range, Inhibition)
            MIC_S.append(IC)
            S_atMIC.append(Growth_IC)
            # Drug per cell
            DiC_Calc = interp1d(A_range, DrugContent/drS, bounds_error=False)
            DiS.append(DiC_Calc(Reference_MIC[MIC_ID]).tolist())
            # Carbon per cell @ MIC in Monoculture
            CiS.append(_calculate_carbon(CarbonContent, A_range, drS, IC))
        # Calculate IC for the R-type (NOTE: R is NOT inhibited, IC == N/A!!!)
        for drR, DrugContent, CarbonContent in zip(R[:,:,-1], Drug_in_R[:,:,-1], Carbon_in_R[:,:,-1]):
            IC, Growth_IC = calculate_MIC(drR, A_range, Inhibition)
            MIC_R.append(IC)
            R_atMIC.append(Growth_IC)
            # Drug per cell
            DiC_Calc = interp1d(A_range, DrugContent/drR, bounds_error=False)
            DiR.append(DiC_Calc(IC).tolist())
            # Carbon per cell @ MIC in Monoculture
            CiR.append(_calculate_carbon(CarbonContent, A_range, drR, IC))
        
    return S, MIC_S, S_atMIC, R, MIC_R, R_atMIC, DiS, DiR, CiS, CiR, Drug_in_S,\
        Drug_in_R, Drug_outside


def calculate_MIC(doseResponse, A_range, Inhibition):
    """
        Calculate minimum concentration inhibiting growth by `Inhibition`,
        given a dose-response profile and drug concentrations used.
    """
    IC_grid = interp1d(doseResponse, A_range, bounds_error=False)
    target_growth = doseResponse[0] *  Inhibition
    return IC_grid(target_growth).tolist(), target_growth

# def calculate_density_MIC(doseResponse, A_range, IC):
#     """
#         Calculate cell density at the minimum inhibitory concentration.
#     """
#     grid = interp1d(A_range, doseResponse, bounds_error=False)
#     return grid(IC).tolist()
