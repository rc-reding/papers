import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import ticker as tkr
from mpl_toolkits.mplot3d import Axes3D, proj3d
from matplotlib.patches import FancyArrowPatch
from .ds_solver import calculate_MIC


mpl.rcParams['text.usetex'] = True


class Arrow3D(FancyArrowPatch):
    """
        Plotting arrow in 3D space. Snippet from:
        https://stackoverflow.com/questions/29188612/arrows-in-matplotlib-using-mplot3d

        Args:
        xs: vector containing initial and final position of the arrow in the X axis.
        ys: vector containing initial and final position of the arrow in the Y axis.
        zs: vector containing initial and final position of the arrow in the Z axis.
        *args: check matplotlib documentation.
        **kwargs: check matplotlib documentation.
    """
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, self.axes.M)
        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        FancyArrowPatch.draw(self, renderer)

    def do_3d_projection(self, renderer=None):
        """ Fixes deprecation imposed in matplotlib 3.5.0"""
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, self.axes.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        return np.min(zs)


def plotHeatMap(A_range, P_range, P_HeatMap, MIC_S, P_Label,
                LabelX=r"Antibiotic (\textmu g/mL)", LabelY=None,
                Reference_MIC_S=None, P_Competitor=None,
                Competitor_Type='Tolerant', N_pumps=0):
    """
        Represent cell density at different drug concentrations AND with
        different parameter values.

        Args:
        A_range: Range of antimicrobial (drug) used. Array-type.
        P_range: Range of values for the parameter `p' tweaked.
        P_HeatMap: MxN matrix containing cell density.
        MIC_S: Minimum Inhibitory Inhibition of the most sensitive type of
        microbe measured in mixed culture conditions.
        P_Label: Name of the parameter tested.
        Label_X: Label used for the X axis of the plot.
        Label_Y: Label used for the Y axis of the plot.
        Reference_MIC_S: Minimum Inhibitory Inhibition of the most sensitive
        type of microbe measured in pure culture conditions.
        P_competitor: Parameter value for the competing species, used as a
        reference.
    """
    fig = plt.figure(figsize=(5, 5))
    ax = plt.gca()  # Create axis (only if I need to manipulate it, and I do).
    ax.tick_params(axis='both', direction='out', labelsize=20)

    plt.pcolormesh(A_range, P_range, P_HeatMap, cmap='bone_r', linewidth=0,
                   shading='auto')
    plt.plot(MIC_S, P_range, linewidth=3, color='black', alpha=0.5)

    if P_Competitor:
        plt.plot(A_range[-1] * -0.005, P_Competitor, marker=5, markersize=6,
                 color='black', alpha=0.05, clip_on=False)
        plt.plot([A_range[0], A_range[-1]], [P_Competitor, P_Competitor],
                 linewidth=1, linestyle='dashed', dashes=(5, 5),
                 color='black')
        if P_Label == 'Km_S' and Competitor_Type == 'Sensitive':
            plt.text(1.5, 0.4, 'Parameter value\nfor species S$_2$',
                     fontsize=18, verticalalignment='center',
                     horizontalalignment='center')

    plt.xlabel(LabelX, fontsize=24)
    plt.ylabel(LabelY, fontsize=24)

    plt.xlim([A_range[0], A_range[-1]])

    fx = lambda x, pos: str(x).rstrip('0').rstrip('.')  # int shown as int when float are in axis.
    fy = lambda y, pos: str(round(y, ndigits=4)).rstrip('0').rstrip('.')  # int shown as int when float are in axis.
    ax.xaxis.set_major_formatter(tkr.FuncFormatter(fx))
    ax.yaxis.set_major_formatter(tkr.FuncFormatter(fy))

    if Reference_MIC_S:
        plt.plot(Reference_MIC_S, P_range, linewidth=3, color='darkgray',
                 alpha=0.5)
        if N_pumps < 1:
            plt.savefig('./img/' + P_Label + '_Competition_' +
                        Competitor_Type + '.eps', bbox_inches='tight',
                        transparent=True)
        elif N_pumps > 0:
            plt.savefig('./img/' + P_Label + '_Competition_' +
                        Competitor_Type + '_' + str(N_pumps) + '_pumps.eps',
                        bbox_inches='tight',
                        transparent=True)
    else:
        plt.savefig('./img/' + P_Label + '.eps', bbox_inches='tight',
                    transparent=True)
    plt.close()


def plot3D_to_2D(A_range, P_range, P_HeatMap, P_MIC,
                 Competitor_Type='Tolerant'):
    """ Surface plot overlaying a mesh plot to summarise the meaning of the
        heat maps used.
    """
    A_grid, P_grid = np.meshgrid(A_range, P_range)
    Z_range = np.ones_like(A_range) * 0.1  # Height for MIC line
    fig = plt.figure(figsize=(10, 8))
    ax = fig.gca(projection='3d')  # Create axis (only if I need to manipulate it, and I do).
    ax.tick_params(axis='both', direction='out', labelsize=11)
    # Plot surface first:
    surf_plot = ax.plot_surface(A_grid, P_grid, P_HeatMap, linestyle='None',
                                shade=False, edgecolor='none', cmap='bone_r',
                                ccount=A_range.shape[0]/2,
                                rcount=P_range.shape[0])
    ax.plot_wireframe(A_grid, P_grid, P_HeatMap, linewidth=0.75,
                      ccount=0, rcount=15, zorder=5)
    ax.plot3D(P_MIC, P_range, Z_range, color='black', zorder=4,
              linewidth=2.25)
    # Annotations
    ax.text(1.25, 1.75, 1.2, 'Dose-response with\nparameter value $n$',
            fontsize=12, verticalalignment='center',
            horizontalalignment='center', backgroundcolor='none')
    arr3D = Arrow3D([1, 0.7], [2, 2], [0.95, 0.35], mutation_scale=15,
                    linewidth=1.25, arrowstyle='-|>', color='k')
    ax.add_artist(arr3D)
    ax.text(1.65, 1.75, 0.6, 'IC$_{90}$ with\nparameter value $n$',
            fontsize=12, verticalalignment='center',
            horizontalalignment='center', backgroundcolor='none')
    arr3D = Arrow3D([1.35, 1.175], [2.25, 2.25], [0.25, 0], mutation_scale=15,
                    linewidth=1.25, arrowstyle='-|>', color='k')
    ax.add_artist(arr3D)
    # Axis style
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False
    ax.xaxis._axinfo['grid'].update({'linewidth': 0.25, 'color': 'gainsboro'})
    ax.yaxis._axinfo['grid'].update({'linewidth': 0.25, 'color': 'gainsboro'})
    ax.zaxis._axinfo['grid'].update({'linewidth': 0.25, 'color': 'gainsboro'})
    # Axis labels
    ax.set_ylabel('Parameter from model', fontsize=16, labelpad=10)
    ax.set_xlabel('Antibiotic Concentration', fontsize=16, labelpad=10)
    ax.set_zlabel('Cell Density', fontsize=16, labelpad=10)
    fx = lambda x, pos: str(x).rstrip('0').rstrip('.')  # int shown as int when float are in axis.
    ax.xaxis.set_major_formatter(tkr.FuncFormatter(fx))
    ax.yaxis.set_major_formatter(tkr.FuncFormatter(fx))
    # Colorbar
    cb = fig.colorbar(surf_plot, ax=ax, ticks=[0.1, 0.9],
                      orientation='horizontal', shrink=0.25, pad=0.075)
    cb.set_label('Cell Density (OD)', size=12)
    # Save
    fig.savefig('./img/3D_HeatMap_' + Competitor_Type + '.eps',
                bbox_inches='tight', pad_inches=0.3)
    plt.close()


def plot_DR_overlap(A_range, Ref_HeatMap, Sens_HeatMap, Res_HeatMap, Inhibition,
                    P_idx=None, N_pumps=0, LabelY=""):
    """
        Plot change in cell density as a function of antimicrobial using
        a reference (monoculture), sensitive competitor, and resistant
        competitor, to illustrate the change in IC90.
    """
    if P_idx is None:
        raise SystemExit("P_idx must be >= 0, but it currently is 'None'.")
        sys.exit()

    # All values between 0--1 to compare with experimental data
    Norm_Reference = Ref_HeatMap[P_idx] / Ref_HeatMap[P_idx, 0]
    Norm_Sens = Sens_HeatMap[P_idx] / Sens_HeatMap[P_idx, 0]
    Norm_Res = Res_HeatMap[P_idx] / Res_HeatMap[P_idx, 0]

    Ref_MIC, _ = calculate_MIC(Norm_Reference, A_range, Inhibition)
    Sens_MIC, _ = calculate_MIC(Norm_Sens, A_range, Inhibition)
    Res_MIC, _ = calculate_MIC(Norm_Res, A_range, Inhibition)

    # Plot
    fig = plt.figure(figsize=(5.5, 5))
    ax = plt.gca()  # Create axis (only if I need to manipulate it, and I do).
    ax.tick_params(axis='both', direction='out', labelsize=20)

    plt.plot(A_range, Norm_Reference, color='lightgrey', linewidth=3,
             label='Pure culture')
    plt.plot(A_range, Norm_Sens, color='black', linewidth=3, alpha=0.75,
             label='Mixed w/ Sensitive')
    plt.plot(A_range, Norm_Res, color='black', linewidth=3, alpha=0.75,
             linestyle='dashed', label='Mixed w/ Non-sensitive')

    # Axis
    ax.set_xlabel(r"Antibiotic (\textmu g/mL)", fontsize=24)
    ax.set_ylabel(LabelY, fontsize=24)
    ax.set_xlim([A_range.min(), A_range.max()])
    ax.set_ylim([0, 1.025])

    # Annotations
    plt.plot([Res_MIC, Sens_MIC], [Inhibition, Inhibition], linestyle='dotted',
             linewidth=0.75, alpha=0.75)
    plt.text(Ref_MIC, Inhibition * 2.2, r"IC$_{%d}$" % int(100 - Inhibition*100),
             fontsize=14, horizontalalignment='center')
    plt.arrow(Ref_MIC, Inhibition * 2, 0, -0.075, color='black',
              head_width=0.02, head_length=0.01, zorder=20)

    # Dose-response is normalised, to density at IC is... 'Inhibition'
    plt.plot(Ref_MIC, Inhibition, color='darkgrey', marker='o', markersize=4)
    plt.plot(Sens_MIC, Inhibition, color='red', marker='o', markersize=4)
    plt.plot(Res_MIC, Inhibition, color='red', marker='o', markersize=4)

    fx = lambda x, pos: str(x).rstrip('0').rstrip('.')  # int shown as int when float are in axis.
    fy = lambda y, pos: str(round(y, ndigits=4)).rstrip('0').rstrip('.')  # int shown as int when float are in axis.
    ax.xaxis.set_major_formatter(tkr.FuncFormatter(fx))
    ax.yaxis.set_major_formatter(tkr.FuncFormatter(fy))

    # Legend
    ax.legend(loc='upper right', frameon=False, fontsize=15)

    # Save figure
    if N_pumps == 0:
        plt.savefig('./img/DR_Comparative.eps', bbox_inches='tight')
    else:
        plt.savefig('./img/DR_Comparative_' + str(N_pumps) + '_pumps.eps',
                     bbox_inches='tight')
    plt.close()


def plot_IC_differences(P_range, Reference_MIC_S, MIC_S, P_Label, LabelX=None,
                        Competitor_Type='Tolerant', N_pumps=0):
    """ Plot difference in MICs between two conditions """
    delta_MIC = np.array(MIC_S) - np.array(Reference_MIC_S)

    fig = plt.figure(figsize=(5, 5))
    ax = plt.gca()  # Create axis (only if I need to manipulate it, and I do).
    ax.tick_params(axis='both', which='both', direction='in', labelsize=20)
    # Plot
    plt.plot(P_range, np.zeros_like(P_range), color='black', alpha=0.75,
             linewidth=0.75, linestyle='dashed', dashes=(5, 5))
    plt.plot(P_range, delta_MIC, color='black', linewidth=4)
    # Axis
    f = lambda x, pos: str(round(x, ndigits=4)).rstrip('0').rstrip('.')  # int shown as int when float are in axis.
    ax.yaxis.set_major_formatter(tkr.FuncFormatter(f))
    ax.xaxis.set_major_formatter(tkr.FuncFormatter(f))
    
    ax.set_xlim(P_range.min() * 0.95, P_range.max() * 1.05)
    # Labels
    plt.xlabel(LabelX, fontsize=22)
    plt.ylabel("Difference in IC$_{90}$\n" + r"S$_1$ (\textmu g/mL)",
               fontsize=22)
    # Save
    if N_pumps < 1:
        plt.savefig('./img/MIC_Difference_' + P_Label + '_' +
                    Competitor_Type + '.eps', bbox_inches='tight')
        plt.close()
    elif N_pumps > 0:
        plt.savefig('./img/MIC_Difference_' + P_Label + '_' +
                    Competitor_Type + '_' + str(N_pumps) + '_pumps.eps',
                    bbox_inches='tight')
        plt.close()


def plot_p_vs_DiffContent(P_range, DiCs, P_Label, Competitor_Type='Tolerant',
                          N_pumps=0, Content='Drug', LabelX='Drug',
                          Competition=True):
    """
        Plot antimicrobial content per cell (drug inside cells, or DIC)
        against parameter range in monoculture and, optionally, mixed culture
        conditions.
    """
    P_range = np.array(P_range)
    
    if Competition is True:
        DiC_mono = DiCs[0]
        DiC_mixed = DiCs[1]
    else:
        DiC_mono = DiCs
    
    if Content == 'Drug':
        F_Label = "DIC"
        Content_Units = r"\textmu g/mL/OD"
    elif Content == 'Carbon':
        F_Label = "Carbon"
        Content_Units = "mg/mL/OD"
    
    fig = plt.figure(figsize=(5, 5))
    ax = plt.gca()  # Create axis (only if I need to manipulate it, and I do).
    ax.tick_params(axis='both', which='both', direction='in', labelsize=20)
    # Plot
    if Competition is True:
        plt.plot([P_range.min(), P_range.max()], np.zeros_like([P_range.min(),
                 P_range.max()]), color='black', alpha=0.75, linewidth=0.75,
                 linestyle='dashed', dashes=(5, 5))
        plt.plot(P_range, np.array(DiC_mono) - np.array(DiC_mixed),
                 color='black', alpha=0.5, linewidth=3)
        # Axes
        plt.ylabel('Difference in relative\n' + Content.lower() +
                   ' content (' + Content_Units + ')', fontsize=24)
        if P_Label == 'Km_S' and Competitor_Type == 'Sensitive':
            # Arrow
            ax.annotate('', xy=(0.625, 0.75), xytext=(0.625,-0.75),
                        arrowprops=dict(arrowstyle='<|-|>'))
            plt.text(0.51, 0.25, 'More drug in pure culture',
                     fontsize=16, verticalalignment='center',
                     horizontalalignment='left')
            plt.text(0.51, -0.25, 'More drug in mixed culture',
                     fontsize=16, verticalalignment='center',
                     horizontalalignment='left')
            # Axes
            ylims = ax.get_ylim()
            ax.set_ylim(-1.2, ylims[1])
    else:
        plt.plot(P_range, DiC_mono, color='darkgray', alpha=0.75, linewidth=3)
        plt.ylabel(Content + ' per S-cell\n(' + Content_Units + ')',
                   fontsize=24)
        # Axes
        ax.grid(True, which='both', alpha=0.25, linestyle='dotted')
    
    # Axes
    plt.xlabel(LabelX, fontsize=22)
    if P_Label == 'Km_S':
        ax.set_xlim(P_range.min() * 0.99975, P_range.max() * 1.00025)
    else:
        ax.xaxis.set_major_locator(tkr.MultipleLocator(0.2))
        ax.set_xlim(P_range.min() * 0.95, P_range.max() * 1.01)
    ax.xaxis.set_ticks_position('both')
    ax.yaxis.set_ticks_position('both')
    
    f = lambda x, pos: str(round(x, ndigits=4)).rstrip('0').rstrip('.')  # int shown as int when float are in axis.
    ax.yaxis.set_major_formatter(tkr.FuncFormatter(f))
    ax.xaxis.set_major_formatter(tkr.FuncFormatter(f))
    
    # Save
    if N_pumps < 1:
        plt.savefig('./img/' + P_Label + '_vs_' + F_Label + '_' +
                    Competitor_Type + '.eps', bbox_inches='tight')
    elif N_pumps > 0:
        plt.savefig('./img/' + P_Label + '_vs_' + F_Label + '_' +
                    Competitor_Type + '_' + str(N_pumps) + '_pumps.eps',
                    bbox_inches='tight')
    
    if P_Label == 'Vmax_S' and Competitor_Type == 'Tolerant':
        # Arrow
        ax.annotate('', xy=(1, 0.35), xytext=(1,-0.85),
                    arrowprops=dict(arrowstyle='<|-|>'))
        plt.text(0.51, 0.45, 'More drug in pure culture',
                 fontsize=16, verticalalignment='center',
                 horizontalalignment='left')
        plt.text(0.51, -0.95, 'More drug in mixed culture',
                 fontsize=16, verticalalignment='center',
                 horizontalalignment='left')
        if N_pumps < 1:
            plt.savefig('./img/' + P_Label + '_vs_' + F_Label + '_' +
                        Competitor_Type + '_Main.eps', bbox_inches='tight')
        elif N_pumps > 0:
            plt.savefig('./img/' + P_Label + '_vs_' + F_Label + '_' +
                        Competitor_Type + '_Main_' + str(N_pumps) +
                        '_pumps.eps', bbox_inches='tight')
    
    plt.close()


def plot_VarContent(time, S_Data, R_Data, P_Label, A_idx=-1,
                    Competitor_Type='Tolerant', N_pumps=0, LabelY='none',
                    LabelX='Time (h)'):
    """
        Plot variation of [Content] over time
    """
    S, S_Di = S_Data  # Matrix: P_value-by-A_conc-by-time
    R, R_Di = R_Data  # Matrix: P_value-by-A_conc-by-time
    dS_Di = np.diff(S_Di[0][A_idx])
    dR_Di = np.diff(R_Di[0][A_idx])
    Units = r"(\textmu g/mL/h per OD)"  # phi = mL / OD / h, Aj = µg/mL, d =  / h, Sj = OD
    Units_Inset = r"(\textmu g/mL per OD)"  # phi = mL / OD / h, Aj = µg/mL, d =  / h, Sj = OD

    fig = plt.figure(figsize=(5.5, 5))
    ax = plt.gca()  # Create axis (only if I need to manipulate it, and I do).
    ax.tick_params(axis='both', which='both', direction='in', labelsize=18)

    # Plot
    plt.plot(time[1:], dS_Di / S[0][A_idx][1:], color='black', linewidth=3,
             label=r'S$_1$ (sensitive)')
    plt.plot(time[1:], dR_Di / R[0][A_idx][1:], color='tomato', linewidth=3,
             label=r'S$_2$ (' + Competitor_Type.lower() + ')')
    plt.plot([time[0], time[-1]], [0, 0], color='black', alpha=0.75,
             linewidth=0.75, linestyle='dashed', dashes=(5, 5))  # Reference

    # Annotations
    if Competitor_Type == 'Tolerant':
        if N_pumps < 1:
            ax.annotate('', xy=(3.25, 0.5), xytext=(3.25, -0.5),
                        arrowprops=dict(arrowstyle='<|-|>'))
            plt.text(3.25, 0.95, 'Flux\n' + r'increases $A_j$', fontsize=14,
                     verticalalignment='center', horizontalalignment='center')
            plt.text(3.25, -0.95, 'Flux\n' + r'increases $A_e$', fontsize=14,
                     verticalalignment='center', horizontalalignment='center')
        else:
            ax.annotate('', xy=(8, 0.5), xytext=(8, -0.5),
                        arrowprops=dict(arrowstyle='<|-|>'))
            plt.text(8, 0.95, 'Flux\n' + r'increases $A_j$', fontsize=14,
                     verticalalignment='center', horizontalalignment='center')
            plt.text(8, -0.95, 'Flux\n' + r'increases $A_e$', fontsize=14,
                     verticalalignment='center', horizontalalignment='center')
    elif Competitor_Type == 'Sensitive':
        ax.annotate('', xy=(6, 0.5), xytext=(6, -0.5),
                    arrowprops=dict(arrowstyle='<|-|>'))
        plt.text(6, 0.95, 'Flux\n' + r'increases $A_j$', fontsize=14,
                 verticalalignment='center', horizontalalignment='center')
        plt.text(6, -0.95, 'Flux\n' + r'increases $A_e$', fontsize=14,
                 verticalalignment='center', horizontalalignment='center')

    # Axes
    plt.ylabel(LabelY + Units, fontsize=22)
    plt.xlabel(LabelX, fontsize=22)
    ax.set_xlim(time[1], time[-1])  # [1] because of np.diff()
    Ymax = np.max([(dS_Di / S[0][A_idx][1:]).max(),
                   (dR_Di / R[0][A_idx][1:]).max()])
    if Competitor_Type == 'Sensitive' and P_Label != 'Km_S':
        ax.set_ylim(-1.35, Ymax * 1.25)  # Ymin is ~0 anyway, so...
    elif N_pumps > 0:
        ax.set_ylim(-1.4, Ymax * 1.6)  # Ymin is ~0 anyway, so...
    else:
        ax.set_ylim(-1.35, Ymax * 1.05)  # Ymin is ~0 anyway, so...

    # Inset
    if Competitor_Type == 'Tolerant':
        if N_pumps < 1:
           inset = ax.inset_axes([0.55, 0.55, 0.425, 0.425])
        else:
           inset = ax.inset_axes([0.575, 0.575, 0.375, 0.375])
    else:
        inset = ax.inset_axes([0.6, 0.575, 0.375, 0.375])
    inset.plot(time, S_Di[0][A_idx] / S[0][A_idx], color='black', linewidth=3)
    inset.plot(time, R_Di[0][A_idx] / R[0][A_idx], color='tomato', linewidth=3)
    inset.tick_params(axis='both', which='both', direction='in', labelsize=16)
    if P_Label == 'Km_S':
        inset.set_yticks([0, 25, 50, 75, 100])
    else:
        inset.set_yticks([0, 50, 100, 150, 200])

    inset.set_ylabel('Relative drug content\n' + Units_Inset, fontsize=14)
    inset.set_xlabel(LabelX, fontsize=14)
    inset.set_xlim(time[0], time[-1])
    Ymax = np.max([(S_Di[0][A_idx] / S[0][A_idx]).max(),
                   (R_Di[0][A_idx] / R[0][A_idx]).max()])
    inset.set_ylim(0, Ymax * 1.05)

    # Legend
    if Competitor_Type == 'Tolerant':
        if N_pumps > 0:
            if P_Label != 'Km_S':
                ax.legend(loc='lower right', bbox_to_anchor=(1, 0.025),
                          frameon=False, fontsize=16)
            else:
                ax.legend(loc='lower right', bbox_to_anchor=(1, 0.225),
                          frameon=False, fontsize=16)
        else:
            ax.legend(loc='lower right', bbox_to_anchor=(1, -0.0125),
                      frameon=False, fontsize=16)
    else:
        ax.legend(loc='lower right', bbox_to_anchor=(1, -0.0175),
                  frameon=False, fontsize=16)

    # Save figure
    if N_pumps > 0:
        plt.savefig('./img/' + P_Label + '_vs_' + Competitor_Type + '_flux_' +
                    str(N_pumps) + '_pumps.eps', bbox_inches='tight')
    else:
        plt.savefig('./img/' + P_Label + '_vs_' + Competitor_Type + '_flux.eps',
                    bbox_inches='tight')
    plt.close()

def plot_OD_vs_MIC(OD_Data, MIC, P_Label, Inc_Time, R_Type=None, Log=False,
                   mixed_culture=False):
    """ """
    if mixed_culture is True:
        OD_Mixed = np.array(OD_Data[1])  # Density in mixed culture
        OD_Data = np.array(OD_Data[0])  # Density in pure culture
        MIC_Mixed = np.array(MIC[1])
        MIC = np.array(MIC[0])
    else:
        OD_Data = np.array(OD_Data)
        MIC = np.array(MIC)
    
    fig = plt.figure(figsize=(5.5, 5))
    ax = plt.gca()  # Create axis (only if I need to manipulate it, and I do).
    ax.tick_params(axis='both', which='both', direction='in', labelsize=18)

    # Plot
    plt.plot(OD_Data, MIC, color='black', linewidth=6, label='Pure culture')
    if mixed_culture is True:
        plt.plot(OD_Mixed, MIC_Mixed, color='darkgrey', linewidth=6,
                 label='Mixed culture')

    # Axes
    plt.xlabel(r'S$_1$-density at IC$_{90}$ (OD)', fontsize=22)
    plt.ylabel(r'IC$_{90}$ (\textmu g/mL)', fontsize=22)

    if mixed_culture is True:
        ax.set_ylim(np.nanmin([MIC_Mixed, MIC]) * 0.995, np.nanmax([MIC_Mixed, MIC]) * 1.005)
        ax.set_xlim(np.nanmin([OD_Mixed, OD_Data]), np.nanmax([OD_Mixed, OD_Data]))
    else:
        ax.set_ylim(np.nanmin(MIC) * 0.995, np.nanmax(MIC) * 1.005)
        ax.set_xlim(np.nanmin(OD_Data), np.nanmax(OD_Data))
        
    f = lambda x, pos: str(round(x, ndigits=4)).rstrip('0').rstrip('.')  # int shown as int when float are in axis.
    ax.yaxis.set_major_formatter(tkr.FuncFormatter(f))
    ax.xaxis.set_major_formatter(tkr.FuncFormatter(f))
    
    # Inset
    if mixed_culture is True:
        if Inc_Time == 72:
            if P_Label.lower() == "vmax":
                inset = ax.inset_axes([0.025, 0.275, 0.425, 0.425])
            elif P_Label.lower() == "km":
                inset = ax.inset_axes([0.025, 0.15, 0.425, 0.425])
            elif P_Label.lower() == "yield":
                inset = ax.inset_axes([0.45, 0.375, 0.425, 0.425])
        elif Inc_Time == 24:
            inset = ax.inset_axes([0.25, 0.55, 0.425, 0.425])
        inset.set_yscale('log')
    else:
      if Inc_Time == 72:
          if P_Label.lower() == "vmax" or P_Label.lower() == "km":
                inset = ax.inset_axes([0.025, 0.55, 0.425, 0.425])
          elif P_Label.lower() == "yield":
                inset = ax.inset_axes([0.4, 0.15, 0.425, 0.425])
      elif Inc_Time == 24:
          if P_Label.lower() == "vmax":
                inset = ax.inset_axes([0.025, 0.55, 0.425, 0.425])
          elif P_Label.lower() == "km":
                inset = ax.inset_axes([0.45, 0.125, 0.385, 0.385])
          elif P_Label.lower() == "yield":
                inset = ax.inset_axes([0.425, 0.125, 0.385, 0.385])
    inset.plot(OD_Data, MIC/OD_Data, color='black', linewidth=4, label='Pure culture')
    if mixed_culture is True:
        inset.plot(OD_Mixed, MIC_Mixed/OD_Mixed, color='darkgrey', linewidth=4,
                   label='Mixed culture')
    
    inset.tick_params(labelsize=12)
    inset.set_xlabel(r'S$_1$-density at IC$_{90}$ (OD)', fontsize=14)
    inset.set_ylabel(r'Drug content @ IC$_{90}$' + '\n' +
                     r'(\textmu g/mL per OD)', fontsize=14)

    inset.yaxis.tick_right()
    inset.yaxis.set_label_position('right')
    
    f = lambda x, pos: str(round(x, ndigits=4)).rstrip('0').rstrip('.')  # int shown as int when float are in axis.
    inset.yaxis.set_major_formatter(tkr.FuncFormatter(f))
    inset.xaxis.set_major_formatter(tkr.FuncFormatter(f))
    # Save figure
    if mixed_culture is True:
        plt.savefig('./img/OD_vs_MIC_' + str(Inc_Time) + 'h_' + R_Type +
                    "_" + P_Label + '.eps', bbox_inches='tight')
    else:
        plt.savefig('./img/OD_vs_MIC_' + str(Inc_Time) + 'h_' +
                    P_Label + '.eps', bbox_inches='tight')
    plt.close()


def plot_OD_vs_Content(OD_range, Content, P_Label, Competitor_Type='Tolerant',
                       N_pumps=0, Content_Type='Carbon', Competition=True):
    """
        Plot relative carbon content per cell against OD to see whether
        growing in pure or mixed culture impacts the uptake of carbon.
    """

    if Content_Type == 'Drug':
        F_Label = "DIC"
        Content_Units = r"\textmu g/mL/OD"
    elif Content_Type == 'Carbon':
        F_Label = "Carbon"
        Content_Units = "mg/mL/OD"

    fig = plt.figure(figsize=(5.5, 5))
    ax = plt.gca()  # Create axis (only if I need to manipulate it, and I do).
    ax.tick_params(axis='both', which='both', direction='in', labelsize=20)
    # Plot
    plt.plot(OD_range[0], Content[0], color='black', alpha=0.5, linewidth=3,
             label='Pure culture')
    if Competition is True:
        plt.plot(OD_range[1], Content[1], color='darkgrey', alpha=0.75,
                 linewidth=3, label='Mixed culture')
    # Axes
    plt.ylabel(Content_Type + ' per S$_{1}$-cell\nat IC$_{90}$ (' + Content_Units + ')',
               fontsize=22)
    plt.xlabel(r'S$_{1}$-cell density (OD)', fontsize=22)

    # Legend
    ax.legend(loc='upper right', frameon=False, fontsize=16)

    # Save
    if N_pumps < 1:
        plt.savefig('./img/' + P_Label + '_vs_' + F_Label + '_' +
                    Competitor_Type + '.eps', bbox_inches='tight')
    elif N_pumps > 0:
        plt.savefig('./img/' + P_Label + '_vs_' + F_Label + '_' +
                    Competitor_Type + '_' + str(N_pumps) + '_pumps.eps',
                    bbox_inches='tight')

    plt.close()

