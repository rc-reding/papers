def calculate_pj(pGamma, Efflux_Pumps):
    """
        This algorithm assumes upto four copies of an efflux pump gene,
        like acr, as per in Reding et al. Mol Biol. Evol. (2021).
        
        Valid for 0 <= Efflux_Pumps <=4, where 0<= pj <=1.
    """
    # One gene, one protein.
    # Non-vectorised form of the algorithm: for pGamma = 1, pj[0] = 2/3
    pj = Efflux_Pumps / (1 + pGamma * Efflux_Pumps)
    return  max([0, pj / (2/3) - 1])
    # Vectorised form looks like this:
    #
    # pj = Efflux_Pumps / (1 + pGamma * Efflux_Pumps)
    # pj = pj / pj[0] - 1
    #
    # Where j gene copies means j-1 proteins, with diminishing returns.


def mixed_culture_ode(t, y, params, Competitor_Type='Tolerant',
                      Efflux_Pumps=0):
    # Extract variables
    S1, S2, C_ext, C_1int, C_2int, A_1int, A_2int, A_ext = y
    # Extract parameters
    Vmax_1, Vmax_2, Km_1, Km_2, yield_1,\
        yield_2, d, Ki, phi_1, phi_2, Epsilon, pVmax, pK, pGamma = params
    
    ### Helper functions ###
    Inhibition_1 = 1 / (1 + (A_1int / Ki)**2)
    Inhibition_2 = 1 / (1 + (A_2int / Ki)**2)
    Uptake_1 = Vmax_1 * C_ext / (Km_1 + C_ext) * Inhibition_1
    
    if Competitor_Type == 'Tolerant' and Efflux_Pumps <= 1:
        Uptake_2 = Vmax_2 * C_ext / (Km_2 + C_ext) # * (1 - Epsilon)
    elif Competitor_Type == 'Sensitive' or Efflux_Pumps > 1:
        Uptake_2 = Vmax_2 * C_ext / (Km_2 + C_ext) * Inhibition_2
    
    # To _monitor_ carbon within cells (as `Uptake` is reset every iteration)
    dC_1intdt = Uptake_1
    dC_2intdt = Uptake_2
    
    ### Core ODEs ###
    dS1dt = Uptake_1 * yield_1 * S1
    dS2dt = Uptake_2 * yield_2 * S2
    dC_extdt = - (Uptake_1 + Uptake_2) * (S1 + S2)
    
    # Calculate efflux
    p_j = calculate_pj(pGamma, Efflux_Pumps)
    A_efflux = (pVmax * p_j) / (pK + p_j)
    
    # Diffusion of drug A
    dA_1intdt = -d * A_1int + phi_1 * S1 * (A_ext - A_1int)
    dA_2intdt = -d * A_2int + S2 * (phi_2 * (A_ext - A_2int) - A_efflux * A_2int)
    dA_extdt = -d * A_ext - phi_1 * (A_ext - A_1int) * S1\
                          - S2 * (phi_2 * (A_ext - A_2int) - A_efflux * A_2int)
    
    return [dS1dt, dS2dt, dC_extdt, dC_1intdt, dC_2intdt,
            dA_1intdt, dA_2intdt, dA_extdt]

def set_ode_params(Vmax_1=1.25, Vmax_2=1.25, Km_1=0.5, Km_2=0.5, yield_1=0.65,
                   yield_2=0.65, d=0.0001, Ki=0.1, phi_1=10.0, phi_2=10.0,
                   Epsilon=0.0, pVmax=100.0, pK=0.1, pGamma=0.5):
    """         
        - Vmax: Maximal carbon uptake rate.
        - Km: Affinity for carbon source, a.k.a. half-saturation parameter.
        - yield: Biomass yield as given by K/c (Monod 1947), where "K" is the
            population size at the equilibrium and "c" the amount of carbon
            supplied.
        - Epsilon: Costs of resistance.
        - pVmax: Maximal pump efflux rate.
        - pK: Affinity for drug A, a.k.a. half-saturation parameter.
        - pGamma: Dimensionless constant controlling expression of pump gene.
    """
    return list([Vmax_1, Vmax_2, Km_1, Km_2, yield_1, yield_2,
                  d, Ki, phi_1, phi_2, Epsilon, pVmax, pK, pGamma])

