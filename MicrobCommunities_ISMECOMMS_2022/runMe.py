#!/usr/bin/env python3

from matplotlib import pyplot as plt
import multiprocessing as mp
import numpy as np

from mdl import set_ode_params
from helper_func import *


# Set values for initial conditions vector u0:
A_Sint = 0.0
A_Rint = 0.0
A_init = 2.0

C_Sint = 0.0
C_Rint = 0.0
C_init = 2.0

S_init = 0.001
R_init = 0.001

u0 = [S_init, R_init, C_init, C_Sint, C_Rint, A_Sint, A_Rint, A_init]

# Generate parameters vector:
t = np.linspace(0, 24, num=100)
p = set_ode_params()

numPoints = 150
InhibitionFactor = 0.9
Inhibition = 1 - InhibitionFactor

# Sensitivity of competing species: 'Tolerant' or 'Sensitive'.
# Efflux_Pumps = 1 means gene present but not expressed. Valid
# copies range from 1 to 4 (see manuscript).
R_Type = 'Tolerant'
Efflux_Pumps = 0  # 1 == pump gen present, but not expressed. Use 1 to 4.

# Growth conditions: 'Monoculture' or 'Competition' (default).
R_Condition = 'Competition'

# Generate reference:
A_range = np.linspace(0, A_init, num=numPoints)

if __name__ == '__main__':
    ### MULTIPROCESSING STEP A: Initialise workers. ###
    CPU_N = mp.cpu_count()  # For multiprocessing.
    pool = mp.Pool(processes=CPU_N)

    ## Vmax ##
    u0[1] = 0.0  # Remove competitor.
    # Generate reference doseResponse profiles varying Vmax_S to generate
    # Reference MIC for S:
    Ref_VmaxS_HeatMap, Ref_VmaxS_MIC_S, Ref_VmaxS_S_atMIC, _, _, _, _,\
        Ref_VmaxS_DiS_MIC, _, Ref_VmaxS_CiS_atMIC, _, _,\
            _, _ = screen_VmaxS(pool, A_range, u0, t, p, Inhibition, numPoints)
    u0[1] = R_init
    u0[0] = 0.0  # Remove competitor.
    # Generate reference doseResponse profiles varying Vmax_S to generate
    # Reference MIC for R:
    _, _, _, _, Ref_VmaxR_MIC_R, _, _, _,\
        _, _, _, _, _, _ = screen_VmaxS(pool, A_range, u0, t, p, Inhibition,
                                        numPoints, R_Type)
    u0[0] = S_init  # Introduce competitor.
    VmaxS_HeatMap, VmaxS_MIC_S, VmaxS_S_atMIC, VmaxR_HeatMap,\
        VmaxR_MIC_R, VmaxR_R_atMIC, VmaxS_range, VmaxS_DiS_MIC,\
            VmaxS_DiR_MIC, VmaxS_CiS_atMIC, VmaxS_CiR_atMIC, VmaxS_DiS, VmaxS_DiR,\
                VmaxS_Denv = screen_VmaxS(pool, A_range, u0, t, p, Inhibition,
                                          numPoints, R_Type, Efflux_Pumps,
                                          R_Condition, Ref_VmaxS_MIC_S)
    
    ## Km ##
    u0[1] = 0.0  # Remove competitor.
    # Generate reference doseResponse profiles varying Km_S to generate
    # Reference MIC for S:
    Ref_KmS_Heatmap, Ref_KmS_MIC_S, Ref_KmS_S_atMIC, _, _, _, _, Ref_KmS_DiS_MIC,_,\
        Ref_KmS_CiS_atMIC, _, _, _, _ = screen_KmS(pool, A_range, u0, t, p,
                                                   Inhibition, numPoints)
    u0[1] = R_init
    u0[0] = 0.0  # Remove competitor.
    # Generate reference doseResponse profiles varying Km_S to generate
    # Reference MIC for R:
    _, _, _, _, Ref_KmR_MIC_R, Ref_KmR_R_atMIC, _, _, _, _, _,\
        _, _, _ = screen_KmS(pool, A_range, u0, t, p, Inhibition, numPoints, R_Type)
    u0[0] = S_init  # Introduce competitor.
    KmS_HeatMap, KmS_MIC_S, KmS_S_atMIC, KmR_HeatMap, KmR_MIC_R, KmR_R_atMIC,\
        KmS_range, KmS_DiS_MIC, KmS_DiR_MIC, KmS_CiS_atMIC, KmS_CiR_atMIC, KmS_DiS,\
            KmS_DiR, KmS_Denv = screen_KmS(pool, A_range, u0, t, p, Inhibition,
                                           numPoints, R_Type, Efflux_Pumps,
                                           R_Condition, Ref_KmS_MIC_S)
    
    ## Yield ##
    u0[1] = 0.0  # Remove competitor.
    # Generate reference doseResponse profiles varying Yield_S to generate
    # Reference MIC for S:
    Ref_YieldS_HeatMap, Ref_YieldS_MIC_S, Ref_YieldS_S_atMIC, _, _, _, _,\
        Ref_YieldS_DiS_MIC, _, Ref_YieldS_CiS_atMIC,\
            _, _, _, _ = screen_YieldS(pool, A_range, u0, t, p, Inhibition,
                                       numPoints)
    u0[1] = R_init
    u0[0] = 0.0  # Remove competitor.
    # Generate reference doseResponse profiles varying Yield_S to generate
    # Reference MIC for R:
    _, _, _, _, Ref_YieldR_MIC_R, Ref_YieldR_R_atMIC, _,\
        _, _, _, _, _, _, _ = screen_YieldS(pool, A_range, u0, t, p, Inhibition,
                                            numPoints, R_Type)
    u0[0] = S_init  # Introduce competitor.
    YieldS_HeatMap, YieldS_MIC_S, YieldS_S_atMIC, YieldR_HeatMap, YieldR_MIC_R,\
        YieldR_R_atMIC, YieldS_range, YieldS_DiS_MIC, YieldS_DiR_MIC,\
        YieldS_CiS_atMIC, YieldS_CiR_atMIC, YieldS_DiS, YieldS_DiR,\
                YieldS_Denv = screen_YieldS(pool, A_range, u0, t, p, Inhibition,
                                            numPoints, R_Type, Efflux_Pumps,
                                            R_Condition, Ref_YieldS_MIC_S)
    # Generate heatmap for dose-response overlap plot,
    # enforce 'Sensitive' competitor
    if R_Type == 'Tolerant':
        Sens_YieldS_HeatMap, _, _, _, _, _, _, _, _,\
            _, _, _, _, _ = screen_YieldS(pool, A_range, u0, t, p, Inhibition,
                                          numPoints, 'Sensitive', Efflux_Pumps,
                                          R_Condition, Ref_YieldS_MIC_S)
    
    ### MULTIPROCESSING STEP B: Close workers. ###
    pool.close()
    pool.join()
    
    """ PLOTTING SECTION """
    
    # HEATMAPS
    plotHeatMap(A_range, VmaxS_range, VmaxS_HeatMap[:, :, -1], VmaxS_MIC_S,
                "Vmax_S", LabelY=r"$\bar{\mathrm{\mu}}_{1}$ (mg/OD/h)",
                Reference_MIC_S=Ref_VmaxS_MIC_S, P_Competitor=p[1],
                Competitor_Type=R_Type, N_pumps=Efflux_Pumps)
    plotHeatMap(A_range, KmS_range, KmS_HeatMap[:, :, -1], KmS_MIC_S, "Km_S",
                LabelY="k$_1$ (mg/mL)", Reference_MIC_S=Ref_KmS_MIC_S,
                P_Competitor=p[3], Competitor_Type=R_Type, N_pumps=Efflux_Pumps)
    plotHeatMap(A_range, YieldS_range, YieldS_HeatMap[:, :, -1], YieldS_MIC_S,
                "Yield_S", LabelY="y$_1$ (OD/mg)", Reference_MIC_S=Ref_YieldS_MIC_S,
                P_Competitor=p[5], Competitor_Type=R_Type, N_pumps=Efflux_Pumps)
    
#     plotHeatMap(A_range, VmaxS_range, VmaxR_HeatMap[:, :, -1], VmaxR_MIC_R,
#                 "Vmax_R", LabelY=r"$\bar{\mathrm{\mu}}_{1}$ (mg/OD/h)",
#                 Reference_MIC_S=Ref_VmaxR_MIC_R, P_Competitor=p[1],
#                 Competitor_Type=R_Type, N_pumps=Efflux_Pumps)
#     plotHeatMap(A_range, KmS_range, KmR_HeatMap[:, :, -1], KmR_MIC_R, "Km_R",
#                 LabelY="k$_1$ (mg/mL)", Reference_MIC_S=Ref_KmR_MIC_R,
#                 P_Competitor=p[3], Competitor_Type=R_Type, N_pumps=Efflux_Pumps)
#     plotHeatMap(A_range, YieldS_range, YieldR_HeatMap[:, :, -1], YieldR_MIC_R,
#                 "Yield_R", LabelY="y$_1$ (OD/mg)", Reference_MIC_S=Ref_YieldR_MIC_R,
#                 P_Competitor=p[5], Competitor_Type=R_Type, N_pumps=Efflux_Pumps)
    
    # DIFFERENCES IN MIC
    plot_IC_differences(VmaxS_range, Ref_VmaxS_MIC_S, VmaxS_MIC_S, "Vmax_S",
                        LabelX=r"$\bar{\mathrm{\mu}}_{1}$ (mg/OD/h)",
                        Competitor_Type=R_Type, N_pumps=Efflux_Pumps)
    plot_IC_differences(KmS_range, Ref_KmS_MIC_S, KmS_MIC_S, "Km_S",
                        LabelX="k$_1$ (mg/mL)", Competitor_Type=R_Type,
                        N_pumps=Efflux_Pumps)
    plot_IC_differences(YieldS_range, Ref_YieldS_MIC_S, YieldS_MIC_S, "Yield_S",
                        LabelX="y$_1$ (OD/mg)", Competitor_Type=R_Type,
                        N_pumps=Efflux_Pumps)
    
    # Plot drug per cell of focal species S, for each trait changed
    plot_p_vs_DiffContent(Ref_VmaxS_MIC_S, list([Ref_VmaxS_DiS_MIC, VmaxS_DiS_MIC]),
                          "Vmax_S", Competitor_Type=R_Type, N_pumps=Efflux_Pumps,
                          Content="Drug", LabelX=r"IC$_{90}^*$ (\textmu g/mL), $\bar{\mu}_{1}$")
    plot_p_vs_DiffContent(Ref_KmS_MIC_S, list([Ref_KmS_DiS_MIC, KmS_DiS_MIC]),
                          "Km_S", Competitor_Type=R_Type, N_pumps=Efflux_Pumps,
                          Content="Drug", LabelX=r"IC$_{90}^*$ (\textmu g/mL), k$_1$")
    plot_p_vs_DiffContent(Ref_YieldS_MIC_S, list([Ref_YieldS_DiS_MIC,
                          YieldS_DiS_MIC]), "Yield_S", Competitor_Type=R_Type,
                          N_pumps=Efflux_Pumps, Content="Drug",
                          LabelX=r"IC$_{90}^*$ (\textmu g/mL), y$_1$")
    # Plot carbon per cell of focal species S, for each trait changed
    plot_OD_vs_Content(list([Ref_VmaxS_S_atMIC, VmaxS_S_atMIC]),
                       list([Ref_VmaxS_CiS_atMIC, VmaxS_CiS_atMIC]), "Vmax_S",
                       Competitor_Type=R_Type, N_pumps=Efflux_Pumps,
                       Content_Type="Carbon")
    plot_OD_vs_Content(list([Ref_KmS_S_atMIC, KmS_S_atMIC]),
                       list([Ref_KmS_CiS_atMIC, KmS_CiS_atMIC]), "Km_S",
                       Competitor_Type=R_Type, N_pumps=Efflux_Pumps,
                       Content_Type="Carbon")
    plot_OD_vs_Content(list([Ref_YieldS_S_atMIC, YieldS_S_atMIC]),
                       list([Ref_YieldS_CiS_atMIC, YieldS_CiS_atMIC]), "Yield_S",
                       Competitor_Type=R_Type, N_pumps=Efflux_Pumps,
                       Content_Type="Carbon")
    # Plot relative change in drug content
    plot_VarContent(t, [VmaxS_HeatMap, VmaxS_DiS], [VmaxR_HeatMap, VmaxS_DiR],
                    "Vmax_S", Competitor_Type=R_Type, N_pumps=Efflux_Pumps,
                    LabelY="Relative drug diffusion\n")
    plot_VarContent(t, [KmS_HeatMap, KmS_DiS], [KmR_HeatMap, KmS_DiR], "Km_S",
                    Competitor_Type=R_Type, N_pumps=Efflux_Pumps,
                    LabelY="Relative drug diffusion\n")
    plot_VarContent(t, [YieldS_HeatMap, YieldS_DiS], [YieldR_HeatMap, YieldS_DiR],
                    "Yield_S", Competitor_Type=R_Type, N_pumps=Efflux_Pumps,
                    LabelY="Relative drug diffusion\n")
    
    # Plot OD vs MIC
    plot_OD_vs_MIC(np.array(Ref_VmaxS_S_atMIC), np.array(Ref_VmaxS_MIC_S), "Vmax", int(t[-1]),)
    plot_OD_vs_MIC(np.array(Ref_KmS_S_atMIC), np.array(Ref_KmS_MIC_S), "Km", int(t[-1]),)
    plot_OD_vs_MIC(np.array(Ref_YieldS_S_atMIC), np.array(Ref_YieldS_MIC_S), "yield", int(t[-1]),)
    
    plot_OD_vs_MIC([Ref_VmaxS_S_atMIC, VmaxS_S_atMIC],
                   [np.array(Ref_VmaxS_MIC_S), np.array(VmaxS_MIC_S)], "Vmax", int(t[-1]),
                   R_Type, Log=True, mixed_culture=True)
    plot_OD_vs_MIC([Ref_KmS_S_atMIC, KmS_S_atMIC],
                   [np.array(Ref_KmS_MIC_S), np.array(KmS_MIC_S)], "Km", int(t[-1]),
                   R_Type, Log=True, mixed_culture=True)
    plot_OD_vs_MIC([Ref_YieldS_S_atMIC, YieldS_S_atMIC],
                   [np.array(Ref_YieldS_MIC_S), np.array(YieldS_MIC_S)], "yield", int(t[-1]),
                   R_Type, Log=True, mixed_culture=True)
    
    # Plot explanatory 3D heat map (don't make it look like the others... MIX)
    if R_Type == 'Tolerant':
        plot3D_to_2D(A_range, YieldS_range, VmaxS_HeatMap[:, :, -1], VmaxS_MIC_S,
                     R_Type)
        plot_DR_overlap(A_range, Ref_YieldS_HeatMap[:, :, -1],
                        Sens_YieldS_HeatMap[:, :, -1], YieldS_HeatMap[:, :, -1],
                        Inhibition, P_idx=65, N_pumps=Efflux_Pumps,
                        LabelY="Species S$_1$-cell density\n(normalised OD)")

